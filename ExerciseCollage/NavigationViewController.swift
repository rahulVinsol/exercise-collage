//
//  NavigationViewController.swift
//  ExerciseCollage
//
//  Created by Rahul Rawat on 23/06/21.
//

import UIKit

class NavigationViewController: UINavigationController {
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return UIDevice.current.userInterfaceIdiom == .pad ? .landscape : .portrait
        }
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            return UIDevice.current.userInterfaceIdiom == .pad ? .landscapeLeft : .portrait
        }
    }
    
    override var shouldAutorotate: Bool { get { true } }
}
