//
//  ViewController.swift
//  ExerciseCollage
//
//  Created by Rahul Rawat on 23/06/21.
//

import UIKit
import PencilKit

class ViewController: BasePhotoViewController {
    
    private var shareButton: UIBarButtonItem!
    private var pickImageButton: UIBarButtonItem!
    private var clearCanvasButton: UIBarButtonItem!
    private var pencilButton: UIBarButtonItem!
    private var printButton: UIBarButtonItem!
    
    private var pencilCanvasView: PKCanvasView!
    private weak var toolPicker: PKToolPicker?
    
    private var imagesPicked = 0
    override var pickableImageLimit: Int {
        get {
            imagesLimit - imagesPicked
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationItem.title = "Collage"
        
        setPencilKitView()
        
        pickImageButton = UIBarButtonItem(image: UIImage(systemName: "filemenu.and.selection"), style: .plain, target: self, action: #selector(pickImage))
        shareButton = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .plain, target: self, action: #selector(handleShare))
        clearCanvasButton = UIBarButtonItem(image: UIImage(systemName: "trash"), style: .plain, target: self, action: #selector(clearCanvas))
        pencilButton = UIBarButtonItem(image: UIImage(systemName: "scribble"), style: .plain, target: self, action: #selector(setDrawing))
        printButton = UIBarButtonItem(image: UIImage(systemName: "printer"), style: .plain, target: self, action: #selector(printCanvas))
        
        clearCanvasButton.tintColor = .red
        
        navigationItem.rightBarButtonItems = [clearCanvasButton, pencilButton, pickImageButton]
        navigationItem.leftBarButtonItems = [printButton, shareButton]
        
        view.addInteraction(UIDropInteraction(delegate: self))
        
        view.addSubview(pencilCanvasView)
        
        enableButtons(enable: false)
    }
    
    private func setPencilKitView() {
        pencilCanvasView = PKCanvasView()
        
        pencilCanvasView.delegate = self
        pencilCanvasView.drawing = PKDrawing()
        pencilCanvasView.alwaysBounceVertical = false
        pencilCanvasView.allowsFingerDrawing = true
        pencilCanvasView.backgroundColor = .clear
        pencilCanvasView.isOpaque = false
        pencilCanvasView.isUserInteractionEnabled = false
        
        pencilCanvasView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        if let window = parent?.view.window, let toolPicker = PKToolPicker.shared(for: window) {
            self.toolPicker = toolPicker
            toolPicker.setVisible(false, forFirstResponder: pencilCanvasView)
            toolPicker.addObserver(pencilCanvasView)
            
            pencilCanvasView.becomeFirstResponder()
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        pencilCanvasView.isUserInteractionEnabled = editing
        toolPicker?.setVisible(editing, forFirstResponder: pencilCanvasView)
        if editing {
            pencilCanvasView.becomeFirstResponder()
        } else {
            pencilCanvasView.resignFirstResponder()
        }
    }
    
    @objc func pickImage() {
        showImagePickingSheet()
    }
    
    @objc func handleShare() {
        guard let image = getCanvasImage() else { return }
        
        let activityViewController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        activityViewController.popoverPresentationController?.barButtonItem = shareButton
        
        present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func clearCanvas() {
        for subView in view.subviews {
            if subView != pencilCanvasView {
                subView.removeFromSuperview()
            } else {
                pencilCanvasView.drawing = PKDrawing()
            }
        }
        
        enableButtons(enable: false)
    }
    
    @objc func setDrawing() {
        setEditing(!isEditing, animated: true)
        
        pencilButton.image = isEditing ? UIImage(systemName: "checkmark.circle") : UIImage(systemName: "scribble")
    }
    
    @objc func printCanvas() {
        guard let image = getCanvasImage() else { return }
        
        let printInfo = UIPrintInfo(dictionary: nil)
        printInfo.outputType = UIPrintInfo.OutputType.general
        printInfo.jobName = "My Print Job"
        
        let printController = UIPrintInteractionController.shared
        printController.printInfo = printInfo
        
        printController.printingItem = image
        
        printController.present(animated: true, completionHandler: nil)
    }
    
    private func getCanvasImage() -> UIImage? {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return image
    }
    
    override func pickedImages(images: [UIImage]) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            let viewFrame = self.view.frame
            
            for image in images {
                self.addImageView(image: image, centerPoint: CGPoint(x: viewFrame.width/2, y: viewFrame.height/2))
            }
            
            if !images.isEmpty {
                self.enableButtons(enable: true)
            }
        }
    }
    
    func addImageView(image: UIImage, centerPoint: CGPoint) {
        let imageView = UIImageView(image: image)
        imageView.isUserInteractionEnabled = true
        self.view.addSubview(imageView)
        
        let draggedItemWidth = max(view.frame.width / 7, 200)
        let draggedItemHeight = draggedItemWidth * image.size.height / image.size.width
        
        imageView.frame = CGRect(x: 0, y: 0, width: draggedItemWidth, height: draggedItemHeight)
        
        imageView.center = centerPoint
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.panImageView))
        panGestureRecognizer.delegate = self
        imageView.addGestureRecognizer(panGestureRecognizer)
        
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchImageView))
        pinchGestureRecognizer.delegate = self
        imageView.addGestureRecognizer(pinchGestureRecognizer)
        
        let rotateGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(self.rotateImageView))
        rotateGestureRecognizer.delegate = self
        imageView.addGestureRecognizer(rotateGestureRecognizer)
        
        imageView.transform.rotated(by: imagesPicked % 2 == 0 ? 25 : -25)
        
        view.sendSubviewToBack(imageView)
        
        imagesPicked += 1
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool { true }
    
    private func enableButtons(enable: Bool) {
        clearCanvasButton?.isEnabled = enable
        printButton?.isEnabled = enable
        shareButton?.isEnabled = enable
    }
}

extension ViewController: UIDropInteractionDelegate {
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        for dragItem in session.items {
            dragItem.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: {[weak self] (obj, err) in
                guard let self = self else { return }
                
                if let err = err {
                    print("Failed to load our dragged item:", err)
                    return
                }
                
                guard let draggedImage = obj as? UIImage else { return }
                
                DispatchQueue.main.async {
                    self.addImageView(image: draggedImage, centerPoint: session.location(in: self.view))
                }
            })
        }
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }
}

extension ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
    
    @objc func panImageView(_ sender: UIPanGestureRecognizer) {
        guard let targetView = sender.view else { return }
        let translation = sender.translation(in: view)
        targetView.center = CGPoint(x: targetView.center.x + translation.x, y: targetView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: view)
        
        bringTargetToFront(state: sender.state, targetView: targetView)
    }
    
    @objc func pinchImageView(_ sender: UIPinchGestureRecognizer) {
        guard let targetView = sender.view else { return }
        targetView.transform = targetView.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
        
        bringTargetToFront(state: sender.state, targetView: targetView)
    }
    
    @objc func rotateImageView(_ sender: UIRotationGestureRecognizer) {
        guard let targetView = sender.view else { return }
        targetView.transform = targetView.transform.rotated(by: sender.rotation)
        sender.rotation = 0
        
        bringTargetToFront(state: sender.state, targetView: targetView)
    }
    
    private func bringTargetToFront(state: UIGestureRecognizer.State, targetView: UIView) {
        if state == .began {
            view.bringSubviewToFront(targetView)
            view.bringSubviewToFront(pencilCanvasView)
        }
    }
}

extension ViewController: PKCanvasViewDelegate {
    func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        enableButtons(enable: true)
    }
}

//extension ViewController: UIDragInteractionDelegate {
//    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
//        let touchedPoint = session.location(in: self.view)
//        if let touchedImageView = self.view.hitTest(touchedPoint, with: nil) as? UIImageView {
//
//            let touchedImage = touchedImageView.image
//
//            let itemProvider = NSItemProvider(object: touchedImage!)
//            let dragItem = UIDragItem(itemProvider: itemProvider)
//            dragItem.localObject = touchedImageView
//            return [dragItem]
//        }
//
//        return []
//    }
//
//    func dragInteraction(_ interaction: UIDragInteraction, willAnimateLiftWith animator: UIDragAnimating, session: UIDragSession) {
//        animator.addCompletion { (position) in
//            if position == .end {
//                session.items.forEach { (dragItem) in
//                    if let touchedImageView = dragItem.localObject as? UIView {
//                        touchedImageView.removeFromSuperview()
//                    }
//                }
//            }
//        }
//    }
//
//    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
//        return UITargetedDragPreview(view: item.localObject as! UIView)
//    }
//}


